#pragma once 

#include <fstream>
#include <string>
#include "string.h"

using namespace std;

class Util{
 public:
    static std::ifstream getStream(string path){
        ifstream stream(path);
        if(!stream) {
            throw std::runtime_error("Non existing PID");
        }
        else {
        return stream;
        }   
    }

    static string ConvertToTime(long int time_in_seconds){

        long int seconds = time_in_seconds%60;
        long int minutes = time_in_seconds/60;
        long int hours = time_in_seconds/60;
        string result;
        result = std::to_string(hours)+" :"+std::to_string(minutes)+" :"+std::to_string(seconds);
        return result;
    }

    static string GetProgressBar(string percentage){

        int normalized_percentage = 0;
        try{
            normalized_percentage = (stof(percentage)/100)*50; // converting from string to float
        }
        catch(...){
            normalized_percentage = 0;
        }

        string result= "[0%%";
        for(int i=0;i<50;i++){
            if (i <= normalized_percentage) {
                result+="|";
            }
            else {
                result+=" ";
            }
        }

        result+=percentage.substr(0,5) + " /100%%]";
        return result;
    }

    static bool is_number(const std::string& s){
        return( strspn( s.c_str(), "0123456789" ) == s.size() );
    }


};
