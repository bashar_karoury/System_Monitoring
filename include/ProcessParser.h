#pragma once 

#include <iostream>
#include <string>
#include <vector>
#include "util.h"
#include <sstream>
#include <iterator>
#include <unistd.h>
#include <dirent.h>
#include <cerrno>
#include <stdexcept>
#include <cstring>
#include <algorithm>
using namespace std;
class ProcessParser {
    public:
        static std::string getCmd(std::string pid);
        static std::vector<std::string> getPidList();
        static std::string getVmSize(std::string pid);
        static std::string getCpuPercent(std::string pid);
        static long int getSysUpTime();
        static std::string getProcUpTime(std::string pid);
        static std::string getProcUser(std::string pid);
        static std::vector<std::string> getSysCpuPercent();
        static std::vector<std::string> getSysCpuPercent(std::string coreNumber);
        static float getSysRamPercent();
        static std::string getSysKernelVersion();
        static int getTotalThreads();
        static int getTotalNumberOfProcesses();
        static int getNumberOfRunningProcesses();
        static int getNumberOfCores();
        static string getOsName();
        static string printCpuStats(std::vector<std::string> values1 , std::vector<std::string> values2);
        static float getSysActiveCpuTime(vector<string> values);
        static float getSysIdleCpuTime(vector<string> values);
};