#pragma once
#include <iostream>
#include <vector>
#include "ProcessParser.h"
using namespace std;

class SysInfo{
 private:
    vector<string> lastCpuStats;
    vector<string> currentCpuStats;
    vector<string> coresStats;
    vector<vector<string>> lastCpuCoresStats;
    vector<vector<string>> currentCpuCoresStats;
    string cpuPercentage;
    float memPercentage;    
    string osName;
    string kernalVersion;
    long upTime;
    int totaProcesses;
    int runningProcesses;
    int threads;

 public:
    SysInfo() {
      /*
      Getting initial info about system
      Initial data for individual cores is set
      System data is set
      */
      this->getOtherCores(ProcessParser::getNumberOfCores());
      this->setLastCpuMeasures();
      this->setAttributes();
      this->osName = ProcessParser::getOsName();
      this->kernalVersion = ProcessParser::getSysKernelVersion();
    }

    void setAttributes() {
        // getting parsed data
        this->memPercentage = ProcessParser::getSysRamPercent();
        this->upTime = ProcessParser::getSysUpTime();
        this->totaProcesses = ProcessParser::getTotalNumberOfProcesses();
        this->runningProcesses = ProcessParser::getNumberOfRunningProcesses();
        this->threads = ProcessParser::getTotalThreads();
        this->currentCpuStats = ProcessParser::getSysCpuPercent();
        this->cpuPercentage = ProcessParser::printCpuStats(this->lastCpuStats, this->currentCpuStats);
        this->lastCpuStats = this->currentCpuStats;
        this->setCpuCoresStats();
}
    void setLastCpuMeasures();
    string getMemoryPercentage()const;
    long getUpTime()const;
    string getThreads()const;
    string getkernalVersion()const;
    string getTotoalProcesses()const;
    string getRunningProcesse()const;
    string getOsName()const;
    string getCpuPercent()const;
    void getOtherCores(int _size);
    void setCpuCoresStats();
    vector<string> getCoreStats()const;
};

string SysInfo::getCpuPercent()const{
    return this->cpuPercentage;
}

string SysInfo::getOsName()const{
    return this->osName;
}

string SysInfo::getkernalVersion()const{
    return this->kernalVersion;
}

string SysInfo::getMemoryPercentage()const{
    return to_string(this->memPercentage);
}

long SysInfo::getUpTime()const{
    return this->upTime;
}

string SysInfo::getTotoalProcesses()const{
    return to_string(this->totaProcesses);
}

string SysInfo::getRunningProcesse()const{
    return to_string(this->runningProcesses);
}

string SysInfo::getThreads()const{
    return to_string(this->threads);
}

void SysInfo::setLastCpuMeasures(){
    this->lastCpuStats = ProcessParser::getSysCpuPercent();
}

void SysInfo::getOtherCores(int _size) {
  //when number of cores is detected, vectors are modified to fit incoming data
  this->coresStats = std::vector<std::string>();
  this->coresStats.resize(_size);
  this->lastCpuCoresStats = std::vector<std::vector<std::string>>();
  this->lastCpuCoresStats.resize(_size);
  this->currentCpuCoresStats = std::vector<std::vector<std::string>>();
  this->currentCpuCoresStats.resize(_size);

  for (int i = 0; i < _size; i++) {
    this->lastCpuCoresStats[i] = ProcessParser::getSysCpuPercent(to_string(i));
  }
}

void SysInfo::setCpuCoresStats() {
  // Getting data from files (previous data is required)
  for (int i = 0; i < this->currentCpuCoresStats.size(); i++) {
    this->currentCpuCoresStats[i] = ProcessParser::getSysCpuPercent(to_string(i));
  }

  for (int i = 0; i < this->currentCpuCoresStats.size(); i++) {
    // after acquirement of data we are calculating every core percentage of usage
   this->coresStats[i] = ProcessParser::printCpuStats(this->lastCpuCoresStats[i],this->currentCpuCoresStats[i]);
  }

  this->lastCpuCoresStats = this->currentCpuCoresStats;
}



// Constructing string for every core data display
std::vector<std::string> SysInfo::getCoreStats() const {

  std::vector<std::string> result = std::vector<std::string>();
  for (int i = 0; i < this->coresStats.size(); i++) {
    std::string temp = ("cpu" + to_string(i) + ":   ");
    float check;

    if (!this->coresStats[i].empty()) {
      check = stof(this->coresStats[i]);
    }
    if (!check || this->coresStats[i] == "nan") {
      return std::vector<std::string>();
    }

    temp += Util::GetProgressBar(this->coresStats[i]);
    result.push_back(temp);
  }
  return result;
}
