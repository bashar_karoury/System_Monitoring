#pragma once
#include <iostream>
#include "ProcessParser.h"

using namespace std;

class Process{

 private:
    string pid;
    string user;
    string cmd;
    string cpu;
    string mem;
    string uptime;

 public:
    Process(string pid){
        this->pid = pid;
        this->user = ProcessParser::getProcUser(pid);
        this->cmd = ProcessParser::getCmd(pid);
        this->cpu = ProcessParser::getCpuPercent(pid);
        this->mem = ProcessParser::getVmSize(pid);
        this->uptime = ProcessParser::getProcUpTime(pid);
    }
    void setPid(int pid);
    string getPid();
    string getUser();
    string getMem();
    string getCmd();
    string getUptime();
    string getCpu();
    string getProcess();

};

void Process::setPid(int pid){
    this->pid = to_string(pid);
}

string Process::getPid(){return pid;}

string Process::getUser(){return user;}

string Process::getMem(){return mem;}

string Process::getCmd(){return cmd;}

string Process::getUptime(){return uptime;}

string Process::getCpu(){return cpu;}

string Process::getProcess(){

this->mem = ProcessParser::getVmSize(pid);
this->uptime = ProcessParser::getProcUpTime(pid);
this->cpu = ProcessParser::getCpuPercent(pid);

return (this->pid.substr(0,5) + "     "
                + this->user.substr(0,5) + "     "
                + this->mem.substr(0,5) + "     "
                + this->cpu.substr(0,5) + "     "
                + this->uptime.substr(0,5) + "     "
                + this->cmd.substr(0,30) +"....."
                );
}

