#include "ProcessParser.h"
#include "constants.h"




using namespace std;


std::string ProcessParser::getCmd(std::string pid){

    string path =Path::basePath()+pid+Path::cmdPath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    while(getline(file_stream,line)){
       // cout<<line<<std::endl;
    }
    return line;
}

std::vector<std::string> ProcessParser::getPidList(){
    // Basically, we are scanning /proc dir for all directories with numbers as their names
  // If we get valid check we store dir names in vector as list of machine pids
  DIR *dir;
  vector<string> container;

  if (!(dir = opendir("/proc"))) {
    throw std::runtime_error(std::strerror(errno));
  }

  while (dirent *dirp = readdir(dir)) {
    // is this a directory?
    if (dirp->d_type != DT_DIR) {
      continue;
    }

    // Is every character of the name a digit?
    if (all_of(dirp->d_name, dirp->d_name + std::strlen(dirp->d_name), [](char c) { return std::isdigit(c); })) {
      container.push_back(dirp->d_name);
    }
  }

  //Validating process of directory closing
  if (closedir(dir)) {
    throw std::runtime_error(std::strerror(errno));
  }
  return container;
}


std::string ProcessParser::getVmSize(std::string pid){
    string path =Path::basePath()+pid+Path::statusPath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    string name("VmData");
    float memory_size;
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,name.size(),name)==0){
            //comparing string with the passed string if equal return 0 
            //here we check if we reach the line with VmData status
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings

            memory_size = stof(values[1])/(float)(1024*1024);   //get the second value and convert from kB to gB
            //std::cout<<memory_size<<std::endl;
            break;                                                    // found the desired value
        }
    }
    return std::to_string(memory_size);
}

 std::string ProcessParser::getCpuPercent(std::string pid){
    string path =Path::basePath()+pid+"/"+Path::statPath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    getline(file_stream,line);

    std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
    std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
    vector<string> values(beg,eos);                    //construct vector with all sliced strings
    //aquiring relevant times for calculation of active occupation of CPU for selected process
    float utime = stof(ProcessParser::getProcUpTime(pid));
    float stime = stof(values[14]); 
    float cutime = stof(values[15]);
    float cstime = stof(values[16]);
    float starttime = stof(values[21]);
    float uptime = ProcessParser::getSysUpTime();
    float freq = sysconf(_SC_CLK_TCK);
    //std::cout<<freq<<std::endl;
    float total_time = utime + stime + cutime + cstime;
    float seconds = uptime - (starttime/freq);
    float result = 100.0*((total_time/freq)/seconds);
    return to_string(result);
 }
 
 std::string ProcessParser::getProcUpTime(std::string pid){
    
    string path =Path::basePath()+pid+"/"+Path::statPath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    float proc_up_time;
    getline(file_stream,line);                         //just one line

    std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
    std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
    vector<string> values(beg,eos);                    //construct vector with all sliced strings
    proc_up_time =float(stof(values[13])/sysconf(_SC_CLK_TCK));
    return to_string(proc_up_time);
 }

 long int ProcessParser::getSysUpTime(){
    string path =Path::basePath()+Path::upTimePath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    float proc_up_time;
    getline(file_stream,line);                         //just one line

    std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
    std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
    vector<string> values(beg,eos);                    //construct vector with all sliced strings
    return stoi(values[0]);
 }

 std::string ProcessParser::getProcUser(std::string pid){

    string path =Path::basePath()+pid+Path::statusPath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    string name("Uids");
    string user_id;
    string result;
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,name.size(),name)==0){
            //comparing string with the passed string if equal return 0 
            //here we check if we reach the line with VmData status
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings

            user_id = values[1];   //get the second value as it contatins the uid
        
            break;                                                    // found the desired value
        }
    }

    path ="/etc/passwd";
    std::ifstream file_stream2 =Util::getStream(path);
    name = ("x:" +user_id);
    while(getline(file_stream2,line)){
        //searching line by line
        if(line.find(name)!=std::string::npos){
            //here we check if we reach the line with the specified userid 
            
            result =line.substr(0,line.find(":"));
            return result ;
        }
    }

    return ""; //if not found 
 }

 int ProcessParser::getNumberOfCores(){
    string path =Path::basePath()+Path::cpuInfoPath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    string name("cpu cores");
    int cpu_cores;
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,name.size(),name)==0){
            //comparing string with the passed string if equal return 0 
            //here we check if we reach the line with cpu cores value
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings

            cpu_cores = stoi(values[3]); 
            break;                                                    // found the desired value
        }
    }
    return cpu_cores;
     

 }

std::vector<std::string> ProcessParser::getSysCpuPercent(){
    string path =Path::basePath()+Path::statPath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    string name ="cpu";
   
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,name.size(),name)==0){
            //comparing string with the passed string if equal return 0 
            //here we check if we reach the line with cpu cores value
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings
            return values;                                                    // return the vector 
        }
    }
    return (vector<string>());
 }


 std::vector<std::string> ProcessParser::getSysCpuPercent(std::string coreNumber){
    string path =Path::basePath()+Path::statPath();
    std::ifstream file_stream =Util::getStream(path);
    std::string line ;
    string name ="cpu"+coreNumber;
   
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,name.size(),name)==0){
            //comparing string with the passed string if equal return 0 
            //here we check if we reach the line with cpu cores value
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings
            return values;                                                    // return the vector 
        }
    }
    return (vector<string>());
 }

 float ProcessParser::getSysActiveCpuTime(vector<string> values){
    return (stof(values[S_USER]) +
            stof(values[S_NICE]) +
            stof(values[S_SYSTEM]) +
            stof(values[S_IRQ]) +
            stof(values[S_SOFTIRQ]) +
            stof(values[S_STEAL]) +
            stof(values[S_GUEST]) +
            stof(values[S_GUEST_NICE])) ;
 }
  
 float ProcessParser::getSysIdleCpuTime(vector<string> values){
      return (stof(values[S_IDLE])+stof(values[S_IOWAIT]));
 }

 std::string ProcessParser::printCpuStats(std::vector<std::string> values1, std::vector<std::string>values2){
     //Cpu stats can be calculated only if you take measures in two different times.
     float active_time = ProcessParser::getSysActiveCpuTime(values2) - ProcessParser::getSysActiveCpuTime(values1);
     float idle_time = ProcessParser::getSysIdleCpuTime(values2) - ProcessParser::getSysIdleCpuTime(values1);
     float total_time = active_time + idle_time;
     float result = (active_time/total_time)*100;
     return to_string(result);
 }

 float ProcessParser::getSysRamPercent(){
    string path =Path::basePath()+Path::memInfoPath();
    std::ifstream file_stream =Util::getStream(path);
    string available_name ="MemAvailable:";
    string free_name ="MemFree:";
    string buffers_name = "Buffers:";

    float available_memory, free_memory, buffers;

    
    string line;
    //getting available_memory
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,available_name.size(),available_name)==0){
            //comparing string with the passed string if equal return 0 
            //here we check if we reach the line with cpu cores value
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings
            available_memory=stof(values[1]);
            break;
        }
    }

    file_stream.seekg(0,ios::beg); // to start reading from file again   // NOT EFFICIENT

    //getting free_memory
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,free_name.size(),free_name)==0){
            //comparing string with the passed string if equal return 0 
            //here we check if we reach the line with cpu cores value
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings
            free_memory=stof(values[1]);
            break;
        }
    }

    file_stream.seekg(0,ios::beg); // to start reading from file again
    //getting buffers
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,buffers_name.size(),buffers_name)==0){
            //comparing string with the passed string if equal return 0 
            //here we check if we reach the line with cpu cores value
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings
            buffers=stof(values[1]);
            break;
        }
    }
        float ram =float((1-(free_memory/(available_memory-buffers))));
    return float(100.0*(1-(free_memory/(available_memory-buffers))));
 }

 std::string ProcessParser::getSysKernelVersion(){

    string path =Path::basePath()+Path::versionPath();
    std::ifstream file_stream =Util::getStream(path);
    string version_name ="Linux version:";
    string version;

    string line;
    //getting version_name
    getline(file_stream,line);
    
    std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
    std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
    vector<string> values(beg,eos);                    //construct vector with all sliced strings
    version=values[2];

    return version; 
 }

 string ProcessParser::getOsName(){
    string path ="/etc/os-release";
    std::ifstream file_stream =Util::getStream(path);
    string name ="PRETTY_NAME=";
    string os_name;

    string line;
    //getting os_name
    while(getline(file_stream,line)){
        //searching line by line
        if(line.compare(0,name.size(),name)==0){
            // std::cout<<"line : "<<line.size()<<std::endl;
            // std::cout<<"name : "<<name.size()<<std::endl;
            os_name=line.substr(name.size()+1,line.size()-name.size()-2); //to exclude quotation marks
            return os_name;
        }
    }
    return "";
 }

 int ProcessParser::getTotalThreads(){
     std::vector<std::string> pid_vector = ProcessParser::getPidList();
     int sum_of_threads = 0;
    for(string pid : pid_vector){
        string path = Path::basePath()+pid+Path::statusPath();
        ifstream stream(path);
        string line;
        string name = "Threads:";
        while(getline(stream,line)){

            if (line.compare(0,name.size(),name)==0){
            istringstream buff(line);
            istream_iterator<string> beg(buff), eos;
            vector<string> values(beg,eos);
            sum_of_threads+=stoi(values[1]);
            break;
            }
        }

    }
    return sum_of_threads;
 }

 int ProcessParser::getTotalNumberOfProcesses(){
    string path =Path::basePath()+Path::statPath();
    std::ifstream file_stream =Util::getStream(path);
    string name ="processes";
    int number_of_processes;

    string line;
  
    while(getline(file_stream,line)){
        if(line.compare(0,name.size(),name)==0){
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings
            number_of_processes=stoi(values[1]);

            return number_of_processes; 
            }
    }
    return 0;
 }

 int ProcessParser::getNumberOfRunningProcesses(){
    string path =Path::basePath()+Path::statPath();
    std::ifstream file_stream =Util::getStream(path);
    string name ="procs_running";
    int number_of_runniong_processes;

    string line;
  
    while(getline(file_stream,line)){
        if(line.compare(0,name.size(),name)==0){
            std::istringstream buffer(line);                   //creating string stream with the line to slice the string further
            std::istream_iterator<string> beg(buffer) ,eos ;   //create 2 iterators one point to start of the stream and the other to the end.
            vector<string> values(beg,eos);                    //construct vector with all sliced strings
            number_of_runniong_processes = stoi(values[1]);

            return number_of_runniong_processes; 
            }
    }
    return 0;
 }